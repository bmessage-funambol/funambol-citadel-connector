/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.citadel.sync;

import com.funambol.framework.core.AlertCode;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.filter.FilterClause;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.tools.Base64;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import net.bionicmessage.funambol.citadel.store.CtdlFnblConstants;
import org.citadel.lite.CitadelToolkit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * A test class for CitadelSyncSource
 * @author matt
 */
public class SyncSourceUpdateTest {

    public static final String CONFIG_FILE_PATH = ".citadelsyncsource_test_properties";
    SyncContext ctx = null;
    static Properties testProps = null;
    CitadelSyncSource css = null;
    Sync4jPrincipal spp = null;
    int serverPort = 504;
    String username = null;
    String password = null;

    public SyncSourceUpdateTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        String testPropFilePath = String.format("%s%s%s",
                System.getProperty("user.home"),
                File.separator,
                CONFIG_FILE_PATH);
        File testPropertiesFile = new File(testPropFilePath);
        if (!testPropertiesFile.exists()) {
            System.err.format("Cannot execute tests as the config file\n%s",
                    testPropFilePath);
            System.err.println(" does not exist. See the documentation for information");
            throw new Exception("Testcases are not configured");
        }
        testProps = new Properties();
        testProps.load(new FileInputStream(testPropertiesFile));
        String storeLocation = String.format("%s%c%s",
                System.getProperty("java.io.tmpdir"),
                File.separatorChar,
                "citadelemailsynctest");
        testProps.setProperty(CtdlFnblConstants.STORE_LOC,
                storeLocation);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        css = new CitadelSyncSource();
        // Keep test properties and sync source properties separate
        Properties syncSourceProperties = new Properties();
        syncSourceProperties.setProperty(CtdlFnblConstants.SERVER_HOST,
                testProps.getProperty(CtdlFnblConstants.SERVER_HOST));
        syncSourceProperties.setProperty(CtdlFnblConstants.SERVER_PORT,
                testProps.getProperty(CtdlFnblConstants.SERVER_PORT));
        syncSourceProperties.setProperty(CtdlFnblConstants.ROOM_BASE + "Mail",
                testProps.getProperty(CtdlFnblConstants.ROOM_BASE + "Mail"));
        syncSourceProperties.setProperty(CtdlFnblConstants.STORE_LOC,
                testProps.getProperty(CtdlFnblConstants.STORE_LOC));
        css.setSyncSourceProperties(syncSourceProperties);
        // Set up a Sync4jPrincipal
        username =  testProps.getProperty(CtdlFnblConstants.USER_NAME);
        spp = spp.createPrincipal(
                username,
                "testDevice");
        password =  testProps.getProperty(CtdlFnblConstants.USER_PASS);
        String authString = String.format("%s:%s",
                testProps.getProperty(CtdlFnblConstants.USER_NAME),
                password);
        byte[] b64auth = Base64.encode(authString.getBytes("UTF-8"));
        spp.setEncodedUserPwd(new String(b64auth, "UTF-8"));
        spp.getUser().setPassword(
                testProps.getProperty(CtdlFnblConstants.USER_PASS));
        SyncSourceInfo ssInfo = new SyncSourceInfo();
        ContentType[] ctypes = new ContentType[]{
            new ContentType("application/vnd.omads-email+xml", "1.2"),
            new ContentType("application/vnd.omads-folder+xml", "1.2")
        };
        ssInfo.setSupportedTypes(ctypes);
        css.setInfo(ssInfo);

        serverPort =
                Integer.parseInt(testProps.getProperty(CtdlFnblConstants.SERVER_PORT));
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNewItems() throws Exception {
        // Set up a test toolkit
        CitadelToolkit toolkit = new CitadelToolkit();

        toolkit.open(testProps.getProperty(CtdlFnblConstants.SERVER_HOST), serverPort);
        toolkit.login(username, password);
        FilterClause cls = new FilterClause();
        ctx = new SyncContext(spp, AlertCode.SLOW,
                cls,
                null,
                0);
        css.beginSync(ctx);
        SyncItemKey[] allKeys = css.getAllSyncItemKeys();
        assertEquals("Number of keys", 2, allKeys.length); // Folders only
        SyncItemKey[] newKeys = css.getNewSyncItemKeys(null, null);
        SyncItemKey[] updatedKeys = css.getUpdatedSyncItemKeys(null, null);
        css.commitSync();
        css.endSync();

        // Add a message
        addMessage(toolkit);

        // Sync again
        css = null;
        ctx = new SyncContext(spp, AlertCode.TWO_WAY,
                cls,
                null,
                0);
        setUp();
        css.beginSync(ctx);
        newKeys = css.getNewSyncItemKeys(null, null);
        assertEquals("Number of new keys", 1, newKeys.length);
        css.commitSync();
        css.endSync();
        // Sync again, check if cleared from new
        css = null;
        setUp();
        css.beginSync(ctx);
        newKeys = css.getNewSyncItemKeys(null, null);
        assertEquals("Number of new keys", 0, newKeys.length);
        css.commitSync();
        css.endSync();

        // Lets try toggling status
        List<String> messages = toolkit.getMessagesInRoom();
        String messageId = messages.get(0);
        toolkit.setSeenStatus(messageId, true);
        css = null;
        setUp();
        css.beginSync(ctx);
        updatedKeys = css.getUpdatedSyncItemKeys(null, null);
        assertEquals("Number of updated keys", 1, updatedKeys.length);
        css.commitSync();
        css.endSync();
        toolkit.deleteMessage(messageId);
        css = null;
        setUp();
        css.beginSync(ctx);
        SyncItemKey[] deletedKeys = css.getDeletedSyncItemKeys(null, null);
        assertEquals("Number of deleted keys", 1, deletedKeys.length);
        SyncItemKey deletedKey = deletedKeys[0];
        assertEquals("Deleted key is correct",messageId,deletedKey.getKeyAsString());
        css.commitSync();
        css.endSync();
        
        toolkit.close();
    }

    private void addMessage(CitadelToolkit toolkit) throws Exception {
        toolkit.postMessage("Mail", "testuser", "Test message",
                "testuser", "", "", "", "Contents");
    }
}
